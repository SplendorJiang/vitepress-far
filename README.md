# vitepress-far
信风所向的[个人网站](https://zhennanwang.cn)的程序代码，基于[vitepress](https://vitepress.vuejs.org/)构建。

### 功能
- 根据文件/目录自动生成侧栏(Sidebar)
- 归档页(/article)
- 标签云页(/tag)
- 简单搜索过滤
- 自定义文章头尾部分内容

### Development
#### Install
`pnpm i`
#### run
`pnpm docs:dev`
#### build
`pnpm docs:build`

生成的制品位于docs/.vitepress/dist/目录

### LINCENSE
[MulanPSL-2.0](http://license.coscl.org.cn/MulanPSL2)
