---
title: Vitepress自定义文章头部之前的内容
date: 2022-09-19
tags:
  - Vue
  - Vitepress
---
# Vitepress自定义文章头部之前的内容
Vitepress默认主题有定义一些插槽，可以在这些插槽里自定义内容
### 扩展默认主题
官方文档参见：https://vitepress.vuejs.org/guide/theme-introduction#extending-the-default-theme
新增如下页面
.vitepress/theme/index.js 
``` js
// .vitepress/theme/index.js
import DefaultTheme from 'vitepress/theme'
import MyLayout from './MyLayout.vue'

export default {
  ...DefaultTheme,
  // override the Layout with a wrapper component that
  // injects the slots
  Layout: MyLayout
}
```
.vitepress/theme/MyLayout.vue
MyLayout是基于原Layout但是使用了#doc-before插槽，这个插槽的内容会出现在文章之前，这里如下示例自定义显示了文章的标题、日期、标签
其他更多的插槽定义参见官方文档
``` vue
<!--.vitepress/theme/MyLayout.vue-->
<script setup>
import DefaultTheme from 'vitepress/theme'
import { useData } from 'vitepress';
import dayjs from 'dayjs'
const { page, frontmatter } = useData();

const { Layout } = DefaultTheme
</script>

<template>
  <Layout>
    <template #doc-before>
      <div style=" padding-top: 20px; padding-bottom: 10px; font-size: 13px;">
        <span class="doc-before-label">标题：</span><span class="doc-before-value">{{ page.frontmatter.title }} </span>
        <span v-if="page.frontmatter?.date" class="doc-before-label" style="margin-left: 5px">日期：</span>
        <span class="doc-before-value">{{ dayjs(frontmatter.date).format("YYYY-MM-DD") }} </span>
        <div v-if="page.frontmatter.tags && page.frontmatter.tags.length" style="padding-top: 10px">
          <span class="doc-before-label">标签：</span>
          <div v-for="item in page.frontmatter.tags" :key="page.frontmatter.title + item" class="tag-text doc-before-value">
            {{ item }}
          </div>
        </div>
      </div>
    </template>
  </Layout>
</template>

<style scoped>
.tag-text {
  font-size: 10px;
  line-height: 1;

  display: inline-block;

  margin-right: 10px;
  padding: 4px 9px;

  text-align: center;

  border: 1px solid #c8c9cc;
  border-radius: 2px;
  background-color: #f4f4f5;
}

.doc-before-label {
  color: var(--vp-c-text-light-3)
}
.dark .doc-before-label {
  color: var(--vp-c-text-dark-3)
}
.doc-before-value {
  color: var(--vp-c-brand-light)
}

</style>
```
