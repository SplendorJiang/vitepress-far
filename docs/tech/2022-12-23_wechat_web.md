---
title: 第三方网站使用微信登录
date: 2022-12-23
tags:
  - 微信
  - 前端
---
# 第三方网站使用微信登录
#### 准备工作
https://open.weixin.qq.com/ 先去申请第三方网站的appId和appSecret
#### 生成微信登录二维码
通过访问此链接可生成微信登录二维码https://open.weixin.qq.com/connect/qrconnect?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_login&state=STATE&lang=cn#wechat_redirect

把APPID和REDIRECT_URI代入，REDIRECT_URI的域名只能为你申请过的第三方网站不能错，http与https均支持，如果要本地调试，可以把hosts文件增加一行127.0.0.1 你的域名。
如果想在你自己的页面中内嵌这个二维码，而不是再跳转页面以免影响体验，可以自己使用iframe引入此链接即可（不必引入官方文档页的js）
lang只有两种选项cn(简体中文)和en(英文),某些需要多语言的场景可能用的着。
#### 扫码后重定向页面要做的工作
扫码成功并确认后会自动定重向跳转到REDIRECT_URI页面，并会在地址中带回code与state参数,然后我们可以把code传回后端，通过https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code

解析出access_token，请注意这个链接只能调用一次，再次调用将会失效
得到access_token后（同时也能获取到openid），可以再通过https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s

得到userinfo（包含昵称、头像url、unionid）,unionid即为微信用户在第三方网站的唯一标识，可以以此判断如果系统中存在unionid即可执行登录逻辑，如不存在可以自动再执行注册逻辑或者返回前端进行绑定手机号等逻辑，看自己需求。
