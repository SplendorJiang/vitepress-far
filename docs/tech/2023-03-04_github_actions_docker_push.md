---
title: Github actions自动构建发布到Docker hub
date: 2023-03-04
tags:
  - github
  - docker
---
# Github actions自动构建发布到Docker hub
[Github actions](https://docs.github.com/zh/actions)是github自带的CI/CD工具，使用yaml文件作为配置，了解其中step的写法后，整个jobs build也容易掌握。本文给出一个项目自动构建docker镜像并发布到Docker hub的示例。

workflow-docker-push.yml
```yml
name: docker push

on:
  release:
    types: [published]
  workflow_dispatch:

jobs:
  build:
    runs-on: ubuntu-latest
    steps:
    - uses: actions/checkout@v3
    - uses: actions/setup-node@v3
      with:
        node-version: 16
    - uses: pnpm/action-setup@v2.2.4
      with:
        version: 7
    - uses: docker/setup-buildx-action@v2
    - name: Login to Docker Hub
      uses: docker/login-action@v2
      with:
        username: ${{ secrets.DOCKERHUB_USERNAME }}
        password: ${{ secrets.DOCKERHUB_TOKEN }}
    - name: Build and push Docker images
      run: |
        pnpm i --no-frozen-lockfile
        pnpm build
        export IMAGETAG=$(cat package.json | jq -r .version)
        docker buildx bake --push
```
#### 这个示例的一些说明和注意事项
- yml文件的位置为.github/workflow/,请注意***一定要在默认分支存在该文件才行***,仅在其他分支创建此文件并不会触发工作流。
- 这里触发条件为发布新发行版，或者也可以手动触发(workflow_dispatch),手动触发为在项目的actions页面选中该工作流后会有run workflow的按钮。
- jobs.build下的steps是个数组，每个元素即为一个步骤，并且有先后顺序，每个步骤至少要有一个uses或run属性值
- 这里node和pnpm都需要指定版本，使用with指定
- 要使用docker push就要先登录到Docker hub，所以有docker/login-action的步骤在前，这里的DOCKERHUB_USERNAME和DOCKERHUB_TOKEN是你的Docker hub登录的账号和token，请先到Docker hub上生成，再到github此项目仓库的设置中去先设置好，项目仓库的设置按钮必须是项目管理员权限才会显示。
- 这里没有给出docker制作镜像的Dockerfile和docker-compose.yml文件示例，这两个文件属于docker知识范畴，这里不讨论。
