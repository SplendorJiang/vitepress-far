---
title: Windows server 2019安装openssh、更改端口
date: 2022-09-27
tags:
  - Windows
  - ssh
---
# Windows server 2019安装openssh、更改端口
### 安装
Windows server 2019已集成openssh，不过只默认安装了client端，没有安装server端。

依次进入Windows设置->应用和功能->管理可选功能->添加功能，选择openssh服务器，一直安装完成就可以了。

安装完成后在服务中就会有两个openssh相关的服务,设置成自动，如果没有启动先启动一下。
![picture 1](https://img.zhennanwang.cn/2022/20220927184325019_.png)


### 配置 更改端口
进入C:\ProgramData\ssh目录，打开sshd_config文件，找到#Port 22，把#注释去掉，更改22为你想要的端口号，然后保存文件，在服务中重启OpenSSH SSH Server即可。

ps: 记得把新端口加入防火墙入站规则。

