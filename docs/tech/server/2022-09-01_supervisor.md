---
title: Linux进程管理工具Supervisor的使用
date: 2022-09-01
tags:
 - linux
 - supervisor
 - 自动重启
---
# Linux进程管理工具Supervisor的使用
[Supervisor](http://http://supervisord.org) 是Linux系统下使用Python开发的一个进程管理工具。它可以很方便的监听、启动、停止、重启一个或多个进程且有简单的web管理页面。用Supervisor管理的进程，当一个进程意外被杀死后，supervisor会自动将它重新拉起，适合用来管理一些需要稳定运行的后台服务。

### 安装
``` bash
apt install supervisor
```
### 配置supervisord.conf
program:后为要监听启动的程序名，这个例子是pocketCrawler
command为执行该程序的命令
directory为执行command之前要转向的目录，不需要的话也不用填
其余更多配置项目可参见官网文档
``` bash
[program:pocketCrawler]
command=node index.mjs
directory=/home/lighthouse/Projects/node/pocket-crawler/                ; directory to cwd to before exec (def no cwd)
```
### 启动
``` bash
supervisord -n -c /etc/supervisor/supervisord.conf
```
### 查看
通过浏览器地址http://127.0.0.1:9001 可以查看当前监听的进程，web页面上还可以进行停止、启动、重启、查看该进程日志等操作。

