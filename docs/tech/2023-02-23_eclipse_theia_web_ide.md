---
title: Eclipse Theia WebIDE在Windows上的架设
date: 2023-02-23
tags:
  - WebIDE
---
# Eclipse Theia WebIDE在Windows上的架设
WebIDE的好处不用说就是浏览器随开随用，之前有[code-server](https://github.com/coder/code-server)但是只能linux上布署，现在Eclipse这边出了个[Theia](https://theia-ide.org/)，除了linux也可以在Windows上架设了。不管是code-server还是Theia其实都是基于vscode的api和ui开发的，可以使用OpenVSX的插件库，对于经常用VSCode的人可以很方便切换，无需额外学习成本。多的不说了，本文只讲解下Widnows的远程服务器上的架设步骤。
#### 准备环境
- Windows 10+/Server 2019+
- nodejs v18
- npm v8
> 只能是npm8的版本,如果已经是9的先降级 `npm i -g npm@next-8`
- yarn
> 官方不支持pnpm，只能用yarn，如果没有先安装 `npm i -g yarn`
- python 3
> 2不行，要安装最新的3 https://www.python.org/
- Windows-Build-Tools
> 安装Visual C++ Build环境: [Visual Studio Build Tools](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools)(选择"Visual C++ build tools"工作负荷安装，其他的可以不装)
 命令行执行`npm config set msvs_version 2017`或者直接在用户.npmrc文件中手动添加一行`msvs_version=2017`

#### 安装步骤
##### 1. 新增一个目录(比如命名为theia)

##### 2. 在此目录中新增package.json文件，内容如下
```json
{
  "private": true,
  "dependencies": {
    "@theia/callhierarchy": "next",
    "@theia/file-search": "next",
    "@theia/git": "next",
    "@theia/markers": "next",
    "@theia/messages": "next",
    "@theia/navigator": "next",
    "@theia/outline-view": "next",
    "@theia/plugin-ext-vscode": "next",
    "@theia/preferences": "next",
    "@theia/preview": "next",
    "@theia/search-in-workspace": "next",
    "@theia/terminal": "next",
    "@theia/vsx-registry": "next"
  },
  "devDependencies": {
    "@theia/cli": "next"
  },
  "scripts": {
    "prepare": "yarn run clean && yarn run build && yarn run download:plugins",
    "clean": "theia clean",
    "build": "theia build --mode development",
    "start": "theia start --plugins=local-dir:plugins --hostname=0.0.0.0 --port=3000",
    "download:plugins": "theia download:plugins"
  },
  "theiaPluginsDir": "plugins",
  "theiaPlugins": {
    "vscode-builtin-extensions-pack": "https://open-vsx.org/api/eclipse-theia/builtin-extension-pack/1.50.1/file/eclipse-theia.builtin-extension-pack-1.50.1.vsix"
  },
  "theiaPluginsExcludeIds": [
    "ms-vscode.js-debug-companion",
    "vscode.extension-editing",
    "vscode.github",
    "vscode.github-authentication",
    "vscode.microsoft-authentication"
  ]
}
```
##### 3. yarn install
这步可能会很长，有非常多的包会安装请耐心等待
##### 4. yarn start
启动成功后，你可以在本机浏览器输入地址 http://远程服务器的ip:3000 就可以访问使用Theia WebIDE了。

![Theia 界面预览](https://img.zhennanwang.cn/2023/0223145053966_theia_1677135046769.png)
