---
title: mybatis自定义TypeHandler的全局注册
date: 2023-02-15
tags:
  - java
  - mybatis
  - springboot
---
# mybatis自定义TypeHandler的全局注册
### 自定义TypeHandler
比如重写一个LocalDateTimeTypeHandler准备代替mybatis默认的
```java
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

/**
 * 覆盖mybatis默认的LocalDateTimeTypeHandler
 */
public class LocalDateTimeTypeHandler extends BaseTypeHandler<LocalDateTime> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, LocalDateTime parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setTimestamp(i, Timestamp.valueOf(parameter));
    }

    @Override
    public LocalDateTime getNullableResult(ResultSet rs, String columnName) throws SQLException {
        Timestamp timestamp = rs.getTimestamp(columnName);
        return getLocalDateTime(timestamp);
    }

    @Override
    public LocalDateTime getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        Timestamp timestamp = rs.getTimestamp(columnIndex);
        return getLocalDateTime(timestamp);
    }

    @Override
    public LocalDateTime getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        Timestamp timestamp = cs.getTimestamp(columnIndex);
        return getLocalDateTime(timestamp);
    }

    private static LocalDateTime getLocalDateTime(Timestamp timestamp) {
        if (timestamp != null) {
            Instant instant = timestamp.toLocalDateTime().atOffset(ZoneOffset.UTC).toInstant();
            return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
        return null;
    }
}
```
### 全局注册
这里介绍两种方式
#### 第一种方式：application.properties/yml 配置
这个方式只能注册一个包下的所有TypeHandler
```ini
mybatis.type-handlers-package=com.maintain.test.type
# 如果你装的是mybatis-plus,则改为
mybatis-plus.type-handlers-package=com.maintain.test.type
```
#### 第二种方式: 使用配置类
新增配置类，这种方式非常灵活，推荐
```java
@Configuration
public class MyTypeHandlerConfiguration {

    @Bean
    ConfigurationCustomizer typeHandlerRegistry() {
        return configuration -> configuration.getTypeHandlerRegistry().register(LocalDateTimeTypeHandler.class);
    }
}
```