---
title: Vue获取package.json中的版本号
date: 2020-10-20
tags:
  - Vue
---
# Vue获取package.json中的版本号
#### 示例代码
``` js
import config from '../../package.json'
console.log(config.version)
```
