import DefaultTheme from 'vitepress/theme'
import HomeContent from './components/HomeContent.vue'
import Tags from './components/Tags.vue'
import Articles from './components/Articles.vue'
import MyLayout from './MyLayout.vue'
import Timeline from './components/Timeline.vue'
import TimelineItem from './components/TimelineItem.vue'
import './custom.css'

export default {
  ...DefaultTheme,
  Layout: MyLayout,
  enhanceApp({ app }) {
    app.component('HomeContent', HomeContent)
    app.component('Tags', Tags)
    app.component('Articles', Articles)
    app.component('Timeline', Timeline)
    app.component('TimelineItem', TimelineItem)
  }
}
