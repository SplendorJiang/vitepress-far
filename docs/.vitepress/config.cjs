const getPages = require('./utils/pages.cjs');
const pages = getPages();

const categorys = [
  { basePath: '/tech', text: '学会', link: null, items: [ {basePath: '/server', text: '服务器', link: null } ] },
  { basePath: '/life', text: '何求', link: null },
  { basePath: '/wander', text: '漫步', link: null },
];
// Multiple Sidebars
const sidebar = {};
categorys.forEach(it => {
  // 剔除第二级子目录的文件
  const children = pages.filter(item => item.regularPath.startsWith(it.basePath) && !it.items?.some(element => item.regularPath.startsWith(it.basePath + element.basePath))).map(item => ({ text: item.frontMatter.title, link: item.regularPath }));

  if (children.length) {
    sidebar[it.basePath] = [{ text: it.text, items: children }, ]
    it.items?.forEach(element => {
      const secondLevelChildren = pages.filter(item => item.regularPath.startsWith(it.basePath + element.basePath)).map(item => ({ text: item.frontMatter.title, link: item.regularPath }));
      element.link = secondLevelChildren[0].link
      sidebar[it.basePath].push({ text: element.text, items: secondLevelChildren})
    })
    // console.log('sidebar[it.basePath]', sidebar[it.basePath])
    it.link = children[0].link
  }
  
});
// console.log('sidebar', sidebar)
// 得到所有的tag
let tags = pages.filter(item => item.frontMatter?.tags).reduce((prev, item) => {
  item.frontMatter.tags.forEach(it => {
    prev[it] = (prev[it] || 0) + 1;
  });
  return prev;
}, {});

/**
 * @type {import('vitepress').UserConfig}
 */
const config = {
  head: [
    ['meta', { name: 'keywords', content: '信风所向' }],
    ['link', { rel: 'icon', href: '/favicon.svg' }],
  ],
  title: '信风所向',
  description: '信风所向的个人网站，记录技术笔记、旅行、生活',
  // ...
  themeConfig: {
    pages,
    tags,
    search: false,
    searchMaxSuggestions: 15,
    nav: [
      { text: '首页', link: '/' },
      ...categorys,
      { text: '归档', link: '/article' },
      { text: '标签', link: '/tag' },
      { text: '关于', link: '/about' },
    ],
    // socialLinks: [
    //   { icon: {
    //     svg: '<svg width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="M11.955.49A12 12 0 0 0 0 12.49a12 12 0 0 0 1.832 6.373L11.838 5.928a.187.14 0 0 1 .324 0l10.006 12.935A12 12 0 0 0 24 12.49a12 12 0 0 0-12-12a12 12 0 0 0-.045 0zm.375 6.467l4.416 16.553a12 12 0 0 0 5.137-4.213z"/></svg>'
    //   }, link: 'https://github.com/vuejs/vitepress' },
    // ],
    sidebar,
    footer: {
      message: '基于<a href="https://vitepress.vuejs.org/" target="_blank" style="color: var(--vp-c-brand)">VitePress</a>',
      // 如果你需要使用本代码，请务必把版权和备案信息改成你自己的
      copyright: '<span style="padding: 0 10px">@2021-present SplendorJiang</span><a href="http://beian.miit.gov.cn/" style="color: var(--vp-c-brand)">赣ICP备20002264号-1</a>'
    }
  }
}

module.exports = config
