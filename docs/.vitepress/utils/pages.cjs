const fs = require('fs')
const matter = require('gray-matter')
const dayjs = require('dayjs')
const path = require('path')
const fg = require('fast-glob')
const { SitemapStream, streamToPromise } = require('sitemap')

function getPages() {
  const linksStream = fg.stream(['docs/**/*.md'])
    .map((filePath) => ({ url: filePath.replace('docs/', '').replace(/\.md$/, '.html') }))

  const sitemapStream = new SitemapStream({ hostname: 'https://zhennanwang.cn' })
  streamToPromise(linksStream.pipe(sitemapStream)).then((sitemap) => {
    fs.writeFileSync(
      path.resolve(__dirname, '../../public/sitemap.xml'),
      sitemap,
    )
  })

  const paths = fg.sync(['docs/*/**/*.md'], { ignore: ['node_modules'] })
  // console.log('paths', paths)
  let pages = paths.map((item) => {
    const content = fs.readFileSync(item)
    const stat = fs.statSync(item)
    const { data } = matter(content)
    const dateString = data?.date ? dayjs(data.date, 'YYYY-MM-DD').format('YYYY-MM-DD') : ''
    return {
      frontMatter: data ? { ...data, date: dateString } : null,
      regularPath: `/${item.replace('.md', '.html').replace('docs/', '')}`,
      relativePath: item.replace('docs/', ''),
      lastUpdated: dayjs(stat.mtime).format("YYYY-MM-DD HH:mm:ss")
    }
  })
  // 没有frontMatter的不记录在pages里
  pages = pages.filter((item) => item.frontMatter)
  // 按时间排序
  pages.sort((a, b) => {
    if (a.frontMatter.date > b.frontMatter.date) {
      return -1
    } else if (a.frontMatter.date < b.frontMatter.date) {
      return 1
    } else {
      return 0
    }
  })
  // console.log('pages', pages)
  return pages
}

module.exports = getPages