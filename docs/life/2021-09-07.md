---
title: 给自己的网站配置一下https访问
date: 2021-09-07
tags:
  - nginx
---
# 给自己的网站配置一下https访问
1. 申请下SSL证书
> 由于我用的都是腾讯云，在DNSPOD域名管理页面直接有免费申请1年SSL的选项（不同的子域名需分别申请），申请后几分钟就能开通，然后就是下载证书。打包的证书文件里有nginx需要的.crt和.key文件。
2. 配置nginx
> 把.crt和.key上传至服务器，方便起见我直接传到/etc/nginx/，也就是默认nginx.conf所在的目录，然后在/etc/nginx/sites-enabled/目录新建了my.conf配置文件，新建server项listen 443配置ssl证书之类的，然后重启nginx就可以https访问本网站了。
``` nginx
server {
    listen 443 ssl;

    #填写绑定证书的域名
    server_name zhennanwang.cn;
    root /var/www/html;
    #证书文件名称
    ssl_certificate 1_zhennanwang.cn_bundle.crt;
    #私钥文件名称
    ssl_certificate_key 2_zhennanwang.cn.key;
    ssl_session_timeout 5m;
    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;

    location / {
        index index.html;
    }
}
```
