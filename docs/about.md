---
alone: true
title: 关于
describe: 关于页面
sidebar: false
---
# 关于本站
我是[信风所向](https://gitee.com/splendorjiang)，为了记录编程学习笔记及生活，也为了练习前端技术而创建本站，本站基于[Vitepress](https://vitepress.vuejs.org/)构建，在此基础上增加了根据文件目录自动生成侧栏、标签汇总页、归档页、搜索过滤功能等以方便使用。

可以在[gitee](https://gitee.com/splendorjiang/vitepress-far)找到本站的程序代码。
